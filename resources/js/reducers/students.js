import {GET_STUDENTS, DELETE_STUDENT, GET_STUDENT_MARKS, DELETE_STUDENT_MARK } from "../actions/types";

const initialState = {
    data : [],
    marks: [],
    loading: true,
    error: {}
}

export default function(state=initialState, action) {
    const {type, payload} = action;

    switch(type){
        case GET_STUDENTS:
            return{
                ...state,
                data : payload,
                loading : false
            }
        case DELETE_STUDENT:
            return{
                ...state,
                data: {
                    ...state.data,
                    data: state.data.data.filter(item => item.uuid !== action.payload),
                },
                loading : false
            }
        case GET_STUDENT_MARKS:
            return{
                ...state,
                marks : payload,
                loading : false
            }
        case DELETE_STUDENT_MARK:
            return{
                ...state,
                marks: {
                    ...state.marks,
                    data: state.marks.data.filter(item => item.uuid !== action.payload),
                },
                loading : false
            }
        default:
            return state;
    }
}