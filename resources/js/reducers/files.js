import {GET_FILES, DELETE_FILE} from "../actions/types";

const initialState = {
    files : [],
    loading: true,
    error: {}
}

export default function(state=initialState, action) {
    const {type, payload} = action;
    
    switch(type){
        case GET_FILES:
            return{
                ...state,
                files : payload,
                loading : false
            }
        case DELETE_FILE:
            return{
                ...state,
                files: {
                    ...state.files,
                    data: state.files.data.filter(item => item.id !== action.payload),
                },
                loading : false
            }
        default:
            return state;
    }
}