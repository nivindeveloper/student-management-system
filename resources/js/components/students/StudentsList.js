import React, { Component } from "react";
import {connect} from "react-redux";
import Pagination from "react-js-pagination";
import { Button, Card, Table, Nav, FormControl } from 'react-bootstrap';
import {  Link } from 'react-router-dom';

import CustomTable from "./CustomTable";

import { getStudents, deleteStudent } from "../../actions/students";

class StudentsList extends Component  {

    constructor() {
        super();
        this.state = {
            searchText: "",
            pageNumber: 1
        }
    }

    componentDidMount() {
        //For listing students
        this.props.getStudents({"page":this.state.pageNumber});
    }

     //For search input field
    handleInputChange(event) {
        this.setState({searchText: event.target.value});
    }

    //For search students
    handleSearch() {
        this.props.getStudents({searchText:this.state.searchText});
    }

    handlePageChange(pageNumber) {
        this.props.getStudents({searchText:this.state.searchText, "page":pageNumber});
    }

    render() {
        const {current_page, per_page, total} = this.props.students;

        return (
            <>
                <Nav variant="pills" className="mt-5 mb-1">
                    <Nav.Item className="form-inline">
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" value={this.state.searchText} onChange={e => this.handleInputChange(e)}/>
                        <Button variant="outline-success" onClick={()=>this.handleSearch()}>Search</Button>
                    </Nav.Item>
                    <Nav>
                        <Link to="/student-add" className="m-0 nav-link">Add New</Link>
                    </Nav>
                </Nav>
                {
                    (this.props.students.hasOwnProperty("data") && this.props.students.data.length > 0) ? (
                        <>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Age</th>
                                        <th>Gender</th>
                                        <th>Reporting Teacher</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.students.data.map((student, index)=>
                                        <CustomTable key={index} data={student} deleteAction={this.props.deleteStudent}/>
                                    )}
                                </tbody>
                            </Table>
                            <Pagination
                                activePage={current_page}
                                itemsCountPerPage={per_page}
                                totalItemsCount={total}
                                onChange={this.handlePageChange.bind(this)}
                                itemClass="page-item"
                                linkClass="page-link"
                                firstPageText="First"
                                lastPageText="Last"
                            />
                        </>
                    ) : (
                        <Card>
                            <Card.Body>No records found.</Card.Body>
                        </Card>
                    )
                }
            </>
        )
    }
}

const mapStateToProps = state =>  ({
    //Set student reducer data
    students: state.studentReducer.data
})

const mapDispatchToProps = (dispatch) => {
    return {
        getStudents: (data) => {
            dispatch(getStudents(data));
        },
        deleteStudent: (id) => {
            dispatch(deleteStudent(id));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StudentsList);