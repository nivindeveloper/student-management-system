import React from "react";
import {connect} from "react-redux";
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import { generateStudentId } from '../../utils/commonFunction';

const CustomTable = ({data, deleteAction}) => {
    
    return (
        <>
            <tr>
                <td>{generateStudentId(data.id)}</td>
                <td>{data.first_name + " " + data.last_name}</td>
                <td>{data.age}</td>
                <td>{data.gender}</td>
                <td>{data.teacher != null ? data.teacher.first_name + " " + data.teacher.last_name : ""}</td>
                <td>
                    <Link className="btn btn-primary mr-1" to={"/student/"+data.uuid} variant="primary">Edit</Link>
                    <Button onClick={()=>deleteAction(data.uuid)} variant="danger">Delete</Button>
                </td>
            </tr>
        </>
    )
};

export default connect(null,{})(CustomTable);