import React from "react";
import { Form, Button } from 'react-bootstrap';
import Joi from 'joi-browser';
import { toast } from "react-toastify";

import { addStudent, getStudentDetails, getReportingTeachers } from '../../actions/students';

class StudentAdd extends React.Component{

    constructor(){
        super();
        this.state = {
            requestStatus: false,
            reportingTeachers: [],
            data: {},
            errorMsg: "",
            successMsg: "",
        }

    }

    componentDidMount() {
        //For listing active teachers
        this.getReportingTeachersListAction();

        //Set student details if uuid exists
        if(this.props.match.params.uuid){
            this.getStudentDetailsAction(this.props.match.params.uuid)
        }
    }

    getReportingTeachersListAction() {
        getReportingTeachers().then(response => {
            if (response.data.length > 0) {
                this.setState({reportingTeachers : response.data});
            }
        });
    }

    getStudentDetailsAction(uuid) {
        getStudentDetails(uuid).then(response => {
            if (response.success) {
                this.setState({data : response.data});
            }
        });
    }

    schema = Joi.object() // Form validation schema object
        .keys({
            first_name: Joi.required().label("Please enter the first name"),
            last_name: Joi.required().label("Please enter the last name"),
            age: Joi.required().label("Please enter the age"),
            gender: Joi.required().label("Please enter the gender"),
            teacher_id: Joi.number().min(1).required().label("Please select the reporting teacher")
        }).unknown(true);

    //Update input change
    handleChange = (event) => {
        const error = { ...this.state.error };
        const errorMassage = this.validationProperty(event);
        if (errorMassage) {
            error[event.target.name] = errorMassage;
        } else {
            delete error[event.target.name];
        }

        const data = { ...this.state.data };
        data[event.target.name] = event.target.value;
        this.setState({data, error});
    };

    validationProperty = (event) => {
        const Obj = { [event.target.name]: event.target.value };
        if (this.schema[event.target.name] != undefined) {
            const schema = { [event.target.name]: this.schema[event.target.name] };
            const { error } = Joi.validate(Obj, schema);
            return error ? error.details[0].message : null;
        } else {
            return null;
        }
    };

    validate = () => {
        const options = { abortEarly: false };

        const form = {
            teacher_id: this.state.data.teacher_id,
            first_name: this.state.data.first_name,
            last_name: this.state.data.last_name,
            age: this.state.data.age,
            gender: this.state.data.gender
        };

        const { error } = Joi.validate(form, this.schema, options);

        if (!error) return null;

        const errors = {};
        for (const item of error.details) errors[item.path[0]] = item.message;
        return errors;
    };

    //Form submit after validation
    submitHandler = (event) => {
        event.preventDefault();
        const error = this.validate();

        this.setState({error: error || {}});

        if (!error) {
            const { data } = this.state;
            addStudent(data).then(response => {
                if (response.success == true) {
                    toast.success(response.message);
                    this.props.history.push('/');
                } else {
                    toast.error(response.message);
                }
            });
        }else {
            toast.error(Object.entries(error)[0][1]);
        }
    }

    render(){
        const { data, reportingTeachers } = this.state;

        return(
            <div>
                <div className="row mt-5">
                    <div className="col-md-6 offset-md-3">
                        <h3>Add Student</h3>
                        <Form onSubmit={this.submitHandler}>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                <Form.Label>First name</Form.Label>
                                <Form.Control placeholder="First Name" name="first_name" value={data.first_name} onChange={this.handleChange}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                <Form.Label>Last name</Form.Label>
                                <Form.Control placeholder="Last Name" name="last_name" value={data.last_name} onChange={this.handleChange}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                <Form.Label>Age</Form.Label>
                                <Form.Control placeholder="Age" type="number" name="age" value={data.age} onChange={this.handleChange}  min="1" max="100"/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                <Form.Label className="mb-2">Gender</Form.Label>
                                <Form.Check
                                    inline
                                    label="Male"
                                    name="gender"
                                    value="male"
                                    type="radio"
                                    id={`inline-radio-1`}
                                    onChange={this.handleChange}
                                    checked={data.gender == "male" ? true : false}
                                />
                                <Form.Check
                                    inline
                                    label="Female"
                                    name="gender"
                                    value="female"
                                    type="radio"
                                    id={`inline-radio-2`}
                                    onChange={this.handleChange}
                                    checked={data.gender == "female" ? true : false}
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1" name="teacher_id" onChange={this.handleChange}>
                                <Form.Label>Reporting Teacher</Form.Label>
                                <Form.Control placeholder="Reporting Teacher" name="teacher_id" as="select" onChange={this.handleChange} value={data.teacher_id}>
                                    <option >Select your option</option>
                                    {reportingTeachers.map(list =>
                                        <option key={list.id} value={list.id} >{list.first_name +" "+ list.last_name}</option>)}
                                </Form.Control>
                            </Form.Group>
                            <Button type="submit">Submit</Button>
                        </Form>
                    </div>
                </div>
            </div>
        )
    }
}

export default StudentAdd;