import React, {Component} from 'react';
import { Container, Nav, Navbar } from 'react-bootstrap';
import {  Link } from 'react-router-dom';

class Header extends Component{
    render() {
        return (
             <Navbar bg="primary" expand="md" sticky="top" variant="dark">
                <Container>
                    <Navbar.Brand href="/">Student Management System</Navbar.Brand>
                    <Nav className="me-auto">
                        <Nav.Item><Link to="/" className="m-0 nav-link font-weight-bold text-white">Manage Students</Link></Nav.Item>
                        <Nav.Item ><Link to="/student-marks" className="m-0 nav-link font-weight-bold text-white">Manage Marks</Link></Nav.Item>
                    </Nav>
                </Container>
            </Navbar>
        );
    }
}

export default Header;