import React from "react";
import { Form, Button, Col } from 'react-bootstrap';
import Joi from 'joi-browser';
import { toast } from "react-toastify";

import { addStudentMark, getStudentMarkDetails, getStudentsList } from '../../actions/students';

class StudentAdd extends React.Component{

    constructor(){
        super();
        this.state = {
            requestStatus: false,
            students: [],
            data: {},
            errorMsg: "",
            successMsg: "",
        }

    }

    componentDidMount() {
        //For listing active teachers
        this.getStudentsListAction();

        //Set student mark details if uuid exists
        if(this.props.match.params.uuid){
            this.getStudentMarkDetailsAction(this.props.match.params.uuid)
        }
    }

    getStudentsListAction() {
        getStudentsList().then(response => {
            if (response.data.length > 0) {
                this.setState({students : response.data});
            }
        });
    }

    getStudentMarkDetailsAction(uuid) {
        getStudentMarkDetails(uuid).then(response => {
            if (response.success) {
                this.setState({data : response.data});
            }
        });
    }

    schema = Joi.object() // Form validation schema object
        .keys({
            student_id: Joi.number().min(1).required().label("Please select the student"),
            term: Joi.required().label("Please select the term"),
            maths: Joi.required().label("Please enter the maths mark"),
            science: Joi.required().label("Please enter the science mark"),
            history: Joi.required().label("Please enter the history mark")
        }).unknown(true);

    //Update input change
    handleChange = (event) => {
        const error = { ...this.state.error };
        const errorMassage = this.validationProperty(event);
        if (errorMassage) {
            error[event.target.name] = errorMassage;
        } else {
            delete error[event.target.name];
        }

        const data = { ...this.state.data };
        data[event.target.name] = event.target.value;

        this.setState({data, error});
    };

    validationProperty = (event) => {
        const Obj = { [event.target.name]: event.target.value };
        if (this.schema[event.target.name] != undefined) {
            const schema = { [event.target.name]: this.schema[event.target.name] };
            const { error } = Joi.validate(Obj, schema);
            return error ? error.details[0].message : null;
        } else {
            return null;
        }
    };

    validate = () => {
        const options = { abortEarly: false };

        const form = {
            student_id: this.state.data.student_id,
            term: this.state.data.term,
            maths: this.state.data.maths,
            science: this.state.data.science,
            history: this.state.data.history,
        };

        const { error } = Joi.validate(form, this.schema, options);

        if (!error) return null;

        const errors = {};
        for (const item of error.details) errors[item.path[0]] = item.message;
        return errors;
    };

    //Form submit after validation
    submitHandler = (event) => {
        event.preventDefault();
        const error = this.validate();

       

        this.setState({error: error || {}});

        if (!error) {
            const { data } = this.state;
            addStudentMark(data).then(response => {
                if (response.success == true) {
                    toast.success(response.message);
                    this.props.history.push('/student-marks');
                } else {
                    toast.error(response.message);
                }
            });
        }else {
            toast.error(Object.entries(error)[0][1]);
        }
    }

    render(){
        const { data, students } = this.state;

        return(
            <div>
                <div className="row mt-5">
                    <div className="col-md-6 offset-md-3">
                        <h3>Add Student Marks</h3>
                        <Form onSubmit={this.submitHandler}>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1" name="student_id" onChange={this.handleChange}>
                                <Form.Label>Student</Form.Label>
                                <Form.Control placeholder="Student" name="student_id" as="select" onChange={this.handleChange} value={data.student_id}>
                                    <option >Select your option</option>
                                    {students.map(list =>
                                        <option key={list.id} value={list.id} >{list.first_name +" "+ list.last_name}</option>)}
                                </Form.Control>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1" name="term" onChange={this.handleChange}>
                                <Form.Label>Term</Form.Label>
                                <Form.Control placeholder="Term" name="term" as="select" onChange={this.handleChange} value={data.term}>
                                    <option >Select your option</option>
                                    <option value="One" >One</option>
                                    <option value="Two" >Two</option>
                                </Form.Control>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                <Form.Label>Subjects</Form.Label>

                                <Form.Group className="mb-1 row">
                                    <Form.Label column md="4">
                                        Maths
                                    </Form.Label>
                                    <Col column md="8">
                                        <Form.Control placeholder="Marks" name="maths" type="number" value={data.maths} onChange={this.handleChange}/>
                                    </Col>
                                </Form.Group>
                                <Form.Group className="mb-1 row">
                                    <Form.Label column md="4">
                                        Science
                                    </Form.Label>
                                    <Col column md="8">
                                        <Form.Control placeholder="Marks" name="science" type="number" value={data.science} onChange={this.handleChange}/>
                                    </Col>
                                </Form.Group>
                                <Form.Group className="mb-3 row">
                                    <Form.Label column md="4">
                                        History
                                    </Form.Label>
                                    <Col column md="8">
                                        <Form.Control placeholder="Marks" name="history" type="number" value={data.history} onChange={this.handleChange}/>
                                    </Col>
                                </Form.Group>
                            </Form.Group>
                            <Button type="submit">Submit</Button>
                        </Form>
                    </div>
                </div>
            </div>
        )
    }
}

export default StudentAdd;