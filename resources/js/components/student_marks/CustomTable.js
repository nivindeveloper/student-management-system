import React from "react";
import {connect} from "react-redux";
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Moment from 'moment';

import { generateStudentMarkId } from '../../utils/commonFunction';

const CustomTable = ({data, deleteAction}) => {
    return (
        <>
            <tr>
                <td>{generateStudentMarkId(data.id)}</td>
                <td>{data.student != null ? data.student.first_name + " " + data.student.last_name : "Not Specified"}</td>
                <td>{data.maths}</td>
                <td>{data.science}</td>
                <td>{data.history}</td>
                <td>{data.term}</td>
                <td>{data.total_marks}</td>
                <td>{Moment(data.created_at).format('DD-MM-YYYY')}</td>
                <td>
                    <Link className="btn btn-primary mr-1" to={"/student-mark-edit/"+data.uuid} variant="primary">Edit</Link>
                    <Button onClick={()=>deleteAction(data.uuid)} variant="danger">Delete</Button>
                </td>
            </tr>
        </>
    )
};

export default connect(null,{})(CustomTable);