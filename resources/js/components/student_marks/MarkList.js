import React, { Component } from "react";
import {connect} from "react-redux";
import Pagination from "react-js-pagination";
import { Button, Card, Table, Nav, FormControl } from 'react-bootstrap';
import {  Link } from 'react-router-dom';

import CustomTable from "./CustomTable";

import { getStudentMarks, deleteStudentMark } from "../../actions/students";

class MarksList extends Component  {

    constructor() {
        super();
        this.state = {
            searchText: "",
            pageNumber: 1
        }
    }

    componentDidMount() {
        //For listing students
        this.props.getStudentMarks({"page":this.state.pageNumber});
    }

     //For search input field
    handleInputChange(event) {
        this.setState({searchText: event.target.value});
    }

    //For search students
    handleSearch() {
        this.props.getStudentMarks({"page":1, searchText:this.state.searchText});
    }

    handlePageChange(pageNumber) {
        this.props.getStudentMarks({searchText:this.state.searchText, "page":pageNumber});
    }

    render() {
        const {current_page, per_page, total} = this.props.studentMarks;

        return (
            <>
                <Nav variant="pills" className="mt-5 mb-1">
                    <Nav.Item className="form-inline">
                        <FormControl type="text" placeholder="Search by term" className="mr-sm-2" value={this.state.searchText} onChange={e => this.handleInputChange(e)}/>
                        <Button variant="outline-success" onClick={()=>this.handleSearch()}>Search</Button>
                    </Nav.Item>
                    <Nav>
                        <Link to="/student-mark-add" className="m-0 nav-link">Add New</Link>
                    </Nav>
                </Nav>
                {
                    (this.props.studentMarks.hasOwnProperty("data") && this.props.studentMarks.data.length > 0) ? (
                        <>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Maths</th>
                                        <th>Science</th>
                                        <th>History</th>
                                        <th>Term</th>
                                        <th>Total Marks</th>
                                        <th>Created On</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.props.studentMarks.data.map((student, index)=>
                                        <CustomTable key={index} data={student} deleteAction={this.props.deleteStudentMark}/>
                                    )}
                                </tbody>
                            </Table>
                            <Pagination
                                activePage={current_page}
                                itemsCountPerPage={per_page}
                                totalItemsCount={total}
                                onChange={this.handlePageChange.bind(this)}
                                itemClass="page-item"
                                linkClass="page-link"
                                firstPageText="First"
                                lastPageText="Last"
                            />
                        </>
                    ) : (
                        <Card>
                            <Card.Body>No records found.</Card.Body>
                        </Card>
                    )
                }
            </>
        )
    }
}

const mapStateToProps = state =>  ({
    //Set student mark reducer data
    studentMarks: state.studentReducer.marks
})

const mapDispatchToProps = (dispatch) => {
    return {
        getStudentMarks: (data) => {
            dispatch(getStudentMarks(data));
        },
        deleteStudentMark: (id) => {
            dispatch(deleteStudentMark(id));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MarksList);