import React, {Component} from 'react';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import { ToastContainer } from "react-toastify";

import Header from './Header';
import Footer from './Footer';
import HomePage from './students/HomePage';
import StudentAdd from './students/StudentAdd';
import StudentMarks from './student_marks/StudentMarks';
import StudentMarkAdd from './student_marks/StudentMarkAdd';

import 'react-toastify/dist/ReactToastify.css';

export default class AppRouter extends React.Component{
    render() {
        return (
            <BrowserRouter>
                <Container fluid>
                    <Header/>
                    <ToastContainer />
                    <Switch>
                        <Route path="/" component={HomePage} exact={true} />
                        <Route path="/student-add" component={StudentAdd} exact={true}/>
                        <Route path="/student/:uuid" component={StudentAdd} exact={true}/>

                        <Route path="/student-marks" component={StudentMarks} exact={true}/>
                        <Route path="/student-mark-add" component={StudentMarkAdd} exact={true}/>
                        <Route path="/student-mark-edit/:uuid" component={StudentMarkAdd} exact={true}/>
                    </Switch>
                    <Footer/>
                </Container>
            </BrowserRouter>
        );
    }
}
