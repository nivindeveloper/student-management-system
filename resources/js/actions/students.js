import axios from "axios";
import {GET_STUDENTS, DELETE_STUDENT, GET_STUDENT_MARKS, POST_ERROR, DELETE_STUDENT_MARK} from "./types";
import { toast } from "react-toastify";

import { request, postRequest } from "../utils/request";

const config = {
    headers: {
        "Content-Type": "application/json"
    }
};

//Get all reporting teacher list action
export async function getReportingTeachers()
{
    try {
        return request("teachers-list")
        .then(result => { 
            return result.data; 
        }).catch(error => { 
            return Promise.reject(error); 
        });
    } catch (error) {
        (error.response && error.response.statusText) ? 
            toast.error(error.response.statusText) : toast.error("Sorry, something went wrong.");
    }
}

//Add student action
export async function addStudent(formData)
{
    try {
        return postRequest("add", formData)
        .then(result => { 
            return result.data; 
        }).catch(error => { 
            return Promise.reject(error); 
        });
    } catch (error) {
        (error.response && error.response.statusText) ? 
            toast.error(error.response.statusText) : toast.error("Sorry, something went wrong.");
    }
}

//Get all student list action
export const getStudents = (formData) => async dispatch => 
{
    const config = {
        headers: {
            "Content-Type": "application/json"
        }
    };
    try {
        const res = await postRequest("list?page="+formData.page, formData);
        dispatch({
            type: GET_STUDENTS,
            payload: res.data
        });
    } catch (error) {
        dispatch({
            type: POST_ERROR,
            payload:{
                msg: error.response.statusText,
                status: error.response.status
            }
        });
    }
}

//Get student details action
export async function getStudentDetails(uuid)
{
    try {
        return request("student/"+uuid)
        .then(result => { 
            return result.data; 
        }).catch(error => { 
            return Promise.reject(error); 
        });
    } catch (error) {
        (error.response && error.response.statusText) ? 
            toast.error(error.response.statusText) : toast.error("Sorry, something went wrong.");
    }
}

//Delete student action
export const deleteStudent = uuid => async dispatch => 
{
    try {
        await axios.delete(`/api/student/${uuid}`);
        dispatch({
            type: DELETE_STUDENT,
            payload: uuid
        })
    } catch (err) {
        dispatch({
            type: POST_ERROR,
            payload:{
                msg: err.response.statusText,
                status: err.response.status
            }
        })
    }
}

//Get all students list action
export async function getStudentsList()
{
    try {
        return request("students-list")
        .then(result => { 
            return result.data; 
        }).catch(error => { 
            return Promise.reject(error); 
        });
    } catch (error) {
        (error.response && error.response.statusText) ? 
            toast.error(error.response.statusText) : toast.error("Sorry, something went wrong.");
    }
}

//Add student mark action
export async function addStudentMark(formData)
{
    try {
        return postRequest("mark-add", formData)
        .then(result => { 
            return result.data; 
        }).catch(error => { 
            return Promise.reject(error); 
        });
    } catch (error) {
        (error.response && error.response.statusText) ? 
            toast.error(error.response.statusText) : toast.error("Sorry, something went wrong.");
    }
}

//Get all student marks action
export const getStudentMarks = (formData) => async dispatch => 
{
    const config = {
        headers: {
            "Content-Type": "application/json"
        }
    };
    try {
        const res = await postRequest("mark-list?page="+formData.page, formData);
        dispatch({
            type: GET_STUDENT_MARKS,
            payload: res.data
        });
    } catch (error) {
        dispatch({
            type: POST_ERROR,
            payload:{
                msg: error.response.statusText,
                status: error.response.status
            }
        });
    }
}


//Get student mark detail action
export async function getStudentMarkDetails(uuid)
{
    try {
        return request("mark-details/"+uuid)
        .then(result => { 
            return result.data; 
        }).catch(error => { 
            return Promise.reject(error); 
        });
    } catch (error) {
        (error.response && error.response.statusText) ? 
            toast.error(error.response.statusText) : toast.error("Sorry, something went wrong.");
    }
}

//Delete student mark action
export const deleteStudentMark = uuid => async dispatch => 
{
    try {
        await axios.delete(`/api/mark/${uuid}`);
        dispatch({
            type: DELETE_STUDENT_MARK,
            payload: uuid
        })
    } catch (err) {
        dispatch({
            type: POST_ERROR,
            payload:{
                msg: err.response.statusText,
                status: err.response.status
            }
        })
    }
}