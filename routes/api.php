<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Student - list/add/edit/delete 
Route::post('/add','StudentsController@store');
Route::post('/list', 'StudentsController@index');
Route::get('/student/{student}', 'StudentsController@show');
Route::delete('/student/{student}','StudentsController@delete');
Route::get('/students-list', 'StudentsController@students_list');

//Teacher - list/add/edit/delete 
Route::get('/teachers-list', 'StudentsController@teachers_list');

//Student Marks - list/add/edit/delete 
Route::post('/mark-add','StudentsController@store_marks');
Route::post('/mark-list', 'StudentsController@marks_index');
Route::get('/mark-details/{studentMark}', 'StudentsController@mark_show');
Route::delete('/mark/{studentMark}','StudentsController@mark_delete');
