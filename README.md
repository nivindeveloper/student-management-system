# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Student Management System
* Version

### Built with
* Laravel 8
* React.js
* Redux

### Set Up
1) Clone the repository

2) Create your environment file

3) Update these settings in the .env file:

a) DB_DATABASE (your local database, i.e. "testdb") b) DB_USERNAME (your local db username, i.e. "root") c) DB_PASSWORD (your local db password, i.e. "")

4) composer install

5) php artisan migrate

6) php artisan db:seed 
(for creating sample students, teachers and marks data)

7) Install Javascript dependencies:

    a) npm install

8) php artisan serve