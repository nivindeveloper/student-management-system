<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use Webpatser\Uuid\Uuid;

class TeachersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create sample teachers data
        $faker = Faker::create();

        foreach (range(1,10) as $index) {
            DB::table('teachers')->insert([
                'uuid' => Uuid::generate(),
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'email' => $faker->email,
                'phone_number' => 987654321,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);
        }

        //Create sample student data
        $faker = Faker::create();
        foreach (range(1,10) as $index) {
            DB::table('students')->insert([
                'uuid' => Uuid::generate(),
                'teacher_id'=> $index,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'age' => mt_rand(10,18),
                'gender' => $faker->randomElement(['male', 'female']),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);
        }

        //Create sample student marks data
        $faker = Faker::create();
        foreach (range(1,10) as $index) {
            DB::table('student_marks')->insert([
                'uuid' => Uuid::generate(),
                'student_id'=> $index,
                'term' => $faker->randomElement(['One', 'Two']),
                'maths' => mt_rand(25,50),
                'science' => mt_rand(25,50),
                'history' => mt_rand(25,50),
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);
        }
    }
}
