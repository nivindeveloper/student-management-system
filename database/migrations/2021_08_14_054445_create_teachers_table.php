<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->id();
            $table->string('uuid', 50)->unique();
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('email',50)->unique();
            $table->string('phone_number',15);
            $table->boolean('status',1)->comment('0->Inactive; 1->Active')->default(1);
            $table->integer('created_by')->default(0);
        	$table->integer('updated_by')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
