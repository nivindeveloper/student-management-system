<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Validator;

use App\Repository\StudentRepository;

use App\Models\Student;
use App\Models\StudentMark;
use App\Models\Teacher;
class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //For get all students list
        $students = app(StudentRepository::class)->listProcess($request);

        return response()->json($students);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $returnArr = [];
        $validator = Validator::make($request->all(),[
            'teacher_id' => 'required',
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'age' => 'required',
            'gender' => 'required',
        ]);
        if(!$validator->fails()){
            $returnArr = app(StudentRepository::class)->saveProcess($request);
        }else{
            $returnArr['success'] = 0;
            $returnArr['message'] = $this->errorFormat($validator->messages());
        }
        return response()->json($returnArr);
    }

    //For getting teachers list
    public function teachers_list(Request $request){
        $teachers = Teacher::select(['id', 'first_name', 'last_name'])->where('status', '1')->orderBy('first_name', 'asc')->get();

        $returnArr['success'] = 1;
        $returnArr['message'] = 'Teachers list found';
        $returnArr['data'] = $teachers->toArray();

        return response()->json($returnArr);
    }

    //For getting student list
    public function students_list(Request $request){
        $students = Student::select(['id', 'first_name', 'last_name'])->where('status', '1')->orderBy('first_name', 'asc')->get();

        $returnArr['success'] = 1;
        $returnArr['message'] = 'Students list found';
        $returnArr['data'] = $students->toArray();

        return response()->json($returnArr);
    }

    /**
     * Display the specified student.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        $returnArr['success'] = 1;
        $returnArr['message'] = 'Success';
        $returnArr['data'] = $student;

        return response()->json($returnArr);
    }

    /**
     * Remove the specified student from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Student $student)
    {
        if($student->delete()) {
            $returnArr['success'] = 1;
            $returnArr['message'] = 'Student details successfully deleted';
            $returnArr['uuid'] = $student->uuid;
        }else{
            $returnArr['success'] = 0;
            $returnArr['message'] = 'Something went wrong';
        }
        return response()->json($returnArr);
    }

    /**
     * Display a listing of the student marks.
     *
     * @return \Illuminate\Http\Response
     */
    public function marks_index(Request $request)
    {
        //For get all students list
        $studentMarks = app(StudentRepository::class)->markListProcess($request);

        return response()->json($studentMarks);
    }


     /**
     * Store a newly created student mark in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_marks(Request $request)
    {
        $returnArr = [];
        $validator = Validator::make($request->all(),[
            'student_id' => 'required',
            'maths' => 'required',
            'science' => 'required',
            'history' => 'required',
            'term' => 'required|max:10',
        ]);
        if(!$validator->fails()){
            $returnArr = app(StudentRepository::class)->saveMarkProcess($request);
        }else{
            $returnArr['success'] = 0;
            $returnArr['message'] = $this->errorFormat($validator->messages());
        }
        return response()->json($returnArr);
    }

     /**
     * Display the specified student mark.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mark_show(StudentMark $studentMark)
    {
        $returnArr['success'] = 1;
        $returnArr['message'] = 'Success';
        $returnArr['data'] = $studentMark;

        return response()->json($returnArr);
    }

    /**
     * Remove the specified student mark from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function mark_delete(StudentMark $studentMark)
    {
        if($studentMark->delete()) {
            $returnArr['success'] = 1;
            $returnArr['message'] = 'Student mark details successfully deleted';
            $returnArr['uuid'] = $studentMark->uuid;
        }else{
            $returnArr['success'] = 0;
            $returnArr['message'] = 'Something went wrong';
        }
        return response()->json($returnArr);
    }
}
