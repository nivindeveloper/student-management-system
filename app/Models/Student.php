<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['teacher_id', 'first_name'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function teacher(){
        $fields = ['id','uuid','first_name','last_name'];
        return $this->belongsTo(Teacher::class)->select($fields);
    }

    public function studentMarks(){
        return $this->hasMany(StudentMark::class, 'student_id');
    }
}
