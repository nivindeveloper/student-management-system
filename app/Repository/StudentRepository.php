<?php
namespace App\Repository;

use App\Services\CommonService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

use App\Models\Student;
use App\Models\StudentMark;


class StudentRepository
{
    // student details save process
    public function saveProcess(Request $request)
    {
        $response = ['success' => false, 'message' => __('Invalid request')];
        try {
            if($request->id){
                $student = Student::find($request->id);
            }else{
                $student = new Student();
                $student->uuid = Uuid::generate();
            }
            $student->teacher_id = $request->teacher_id;
            $student->first_name = $request->first_name;
            $student->last_name = $request->last_name;
            $student->age = $request->age;
            $student->gender = $request->gender;
            if ($student->save()){
                $response = [
                    'success' => true,
                    'message' => __("Student data successfully " . ($request->id ? "updated" : "added") . "")
                ];
            }else{
                $response = [
                    'success' => false,
                    'message' => __('Failed to save')
                ];
            }
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage()
            ];
            return $response;
        }

        return $response;
    }

    // student details list process
    public function listProcess($request)
    {
        $select = ["id", "uuid", "teacher_id", "first_name", "last_name", "age", "gender"];
        $students = Student::select($select)->with(['teacher'])->where(
            function($q) use ($request){
                if($request->has('searchText') && !empty(request('searchText'))){
                    $q->orWhere('first_name','LIKE','%' . request('searchText') . '%')->orWhere('last_name','LIKE','%' . request('searchText') . '%');
                }
        })->orderBy('id', 'desc')->paginate(10);

        return $students;
    }

    // student mark details save process
    public function saveMarkProcess(Request $request)
    {
        $response = ['success' => false, 'message' => __('Invalid request')];

        try {
            //Delete existing marks
            if($request->id){
                $existMarks = StudentMark::findOrFail($request->id)->delete();
            }

            $studentMark = new StudentMark();
            $studentMark->uuid = Uuid::generate();
            $studentMark->student_id = $request->student_id;
            $studentMark->term = $request->term;
            $studentMark->maths = $request->maths;
            $studentMark->science = $request->science;
            $studentMark->history = $request->history;
            if ($studentMark->save()){
                $response = [
                    'success' => true,
                    'message' => __("Student Mark data successfully " . ($request->id ? "updated" : "added") . "")
                ];
            }else{
                $response = [
                    'success' => false,
                    'message' => __('Failed to save')
                ];
            }
        } catch (\Exception $e) {
            $response = [
                'success' => false,
                'message' => $e->getMessage()
            ];
            return $response;
        }

        return $response;
    }

    // student mark details list process
    public function markListProcess($request)
    {
        $select = ["id", "uuid", "student_id", "term", "maths", "science", "history", DB::raw('sum(maths + science + history) as total_marks', "created_at")];
        $studentMarks = StudentMark::select($select)->with(['student'])->where(
            function($q) use ($request){
                if($request->has('searchText') && !empty(request('searchText'))){
                    $q->where('term','LIKE','%' . request('searchText') . '%');
                }
        })->orderBy('id', 'desc')->groupBy('id','term')->paginate(10);

        return $studentMarks;
    }
}
